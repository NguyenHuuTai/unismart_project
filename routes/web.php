<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;



Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');



Route::middleware('auth')->group(function(){
    Route::get('dashboard','DashboardController@show');

    Route::get('admin','DashboardController@show');

    Route::get('admin/user/list','AdminUserController@list');

    Route::get('admin/user/add','AdminUserController@add');

    Route::post('admin/user/store','AdminUserController@store');

    Route::get('admin/user/delete/{id}','AdminUserController@delete')->name('delete_user');

    Route::get('admin/user/action','AdminUserController@action');
});
